import {
  defineConfig,
  presetUno,
  presetIcons,
  presetWebFonts,
  transformerDirectives,
  transformerVariantGroup,
  presetTypography,
} from "unocss";

export default defineConfig({
  transformers: [transformerDirectives(), transformerVariantGroup()],
  presets: [
    presetUno(),
    presetIcons(),
    presetWebFonts({
      provider: "none",
      fonts: {
        serif: "Wittgenstein Variable",
        sans: "Gabarito Variable",
        mono: "PT Mono",
      },
    }),
    presetTypography({
      cssExtend: {
        pre: {
          background: "#fafafa !important",
        },
        code: {
          color: "#272727",
        },
      },
    }),
  ],
  shortcuts: {
    section: "px-4 md:px-8 max-w-4xl mx-auto",
    "headline-1": "text-5xl md:text-6xl",
    "headline-2": "text-4xl md:text-5xl",
    "headline-3": "text-3xl md:text-4xl",
    "headline-4": "text-2xl md:text-3xl",
    "headline-5": "text-xl md:text-2xl",
    "headline-6": "text-lg md:text-xl",
  },
  theme: {
    fontSize: {
      "3xs": ["10px", "150%"],
      "2xs": ["12px", "145%"],
      xs: ["14px", "140%"],
      sm: ["16px", "135%"],
      base: ["18px", "130%"],
      lg: ["20px", "125%"],
      xl: ["24px", "120%"],
      "2xl": ["26px", "115%"],
      "3xl": ["28px", "110%"],
      "4xl": ["30px", "105%"],
      "5xl": ["32px", "100%"],
    },
  },
});
