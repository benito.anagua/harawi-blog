// @ts-check
import { defineConfig } from "astro/config";
import UnoCSS from "unocss/astro";
import mdx from "@astrojs/mdx";

// https://astro.build/config
export default defineConfig({
  outDir: "public",
  publicDir: "static",
  site: "https://example.com",
  integrations: [
    UnoCSS({
      injectReset: true,
    }),
    mdx(),
  ],
  redirects: {
    "/blog": "/",
  },
});
