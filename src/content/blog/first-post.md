---
title: "Renacer en cada amanecer"
description: "Lorem ipsum dolor sit amet"
pubDate: "Jul 08 2022"
heroImage: "/placeholder.jpg"
---

# Hola

```
Nuevas oportunidades con un nuevo día,
otro afán en un nuevo sentimiento,
aún así el ayer sigue presenté.
El pasado no se queda atrás,
tus afectos luchan por permanecer,
las puertas se abren frente a ti.
```

```
Ya no estás solo en la marcha,
los que junto a ti están te siguen,
aunque tu paso es tambaleante.
Incluso si no avanzas se marca el camino,
quienes te acompañan ven tu ejemplo,
desde este punto siempre tendrás compañía.
```

```
No ve es oscuras,
más no trae lluvia,
desaliento.
```
